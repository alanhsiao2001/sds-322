# SDS 322 
Objective:
The project aimed to predict fine particulate matter (PM2.5) concentrations across the U.S. EPA’s monitoring network using various environmental and meteorological predictors.

Dataset and Variables:
The dataset included variables like CMAQ, aerosol optical depth (aod), log-transformed road lengths, PM2.5 emissions, and impervious surface measures. These variables were analyzed for their correlation with PM2.5 levels and used to create interaction terms for nuanced modeling.

Methodology:
The team performed a detailed correlation analysis to select variables for modeling. Four predictive models were developed and assessed:

Random Forest
Linear Regression
Ensemble Model (combining Random Forest and XGBoost)
Feedforward Neural Network
All models were rigorously tested using cross-validation to compute the Root Mean Square Error (RMSE) and fine-tuned for robust predictive accuracy evaluations.

